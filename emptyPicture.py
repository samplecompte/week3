# Name: Sam LeCompte
# Date: 9/16/15
# Empty Image

maroon = makeColor(125,0,25)
purple = makeColor(125,0,125)
sqWidth = 50
sqHeight = 50

board = makeEmptyPicture(400,400,maroon)

for x in range(0,8):
  if (x % 2) == 0:
    xEven = True
  else:
    xEven = False 
  
  for y in range(0,8):
    if(y % 2) == 0:
      yEven = True
    else:
      yEven = False
      
    if(xEven and not yEven) or( not xEven and yEven):

      addRectFilled(board,x*sqWidth,y*sqHeight,sqWidth,sqHeight,purple)
show(board)