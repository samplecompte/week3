#Name: Sam LeCompte
#Date: 9/15/15
#Color experiment

def decreaseRed(img):
  for pixel in getPixels(img):
    redVal = getRed(pixel)/2
    setRed(pixel,redVal)
file = "/Users/slecompte2016/Documents/Week3/galaxyImage.jpeg"
picture = makePicture(file)
show(picture)

decreaseRed(picture)

repaint(picture)