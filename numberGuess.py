#Sam LeCompte
#9/14/15
#Number Guess

import random
playAgain = True

while(playAgain):
    target = random.randint(1,100)
    guessCount = 0
    guess = 0

    while(guess!=target and guessCount<5):
        guessCount += 1
        guess = int(input("Pick a number between 1 and 100. "))

        if(guess==target):
            print("Nice guess, way to go!")
        elif(guess>target):
            print("That guess was way too high.")
        else:
            print("That guess was low. ")
    if(guess==target):
        print("Congrats you did it!")
        print("You finished in", guessCount, "guesses.")
    else:
        print("Sorry, you weren't able to guess the number.")

    ask = input("Play again?")

    if(ask == "N" or ask == "n"):
        playAgain==False 
        
    
